//
//  ListCompaniesViewController.swift
//  Empresas
//
//  Created by Victor Vasconcelos on 11/12/19.
//  Copyright © 2019 VictorVasconcelos. All rights reserved.
//

import Foundation
import UIKit
import Combine

class ListCompaniesViewController: UIViewController {
    @IBOutlet weak var companiesTableView: UITableView!
    @IBOutlet weak var logoImageNavbar: UIImageView!
    @IBOutlet weak var searchButton: UIButton!
    
    @IBOutlet weak var searchBarView: UIView!
    @IBOutlet weak var searchSmallLogo: UIImageView!
    @IBOutlet weak var searchBarTextField: UITextField!
    
    private var enterpriseList = EnterpriseList.shared
    private var enterpriseSelected: EnterpriseInfos?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(newEnterpriseListComing),
            name: .newEnterpriseList, object: nil)
        hideKeyboardWhenTappedAround()
        self.searchBarTextField.delegate = self as? UITextFieldDelegate
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @objc func newEnterpriseListComing() {
        DispatchQueue.main.async {
            self.companiesTableView.reloadData()
        }
    }
    
    @IBAction func searchCancelPressed(_ sender: Any) {
        hideSearchBarView()
    }
    
    @IBAction func searchButtonPressed(_ sender: Any) {
        showSearchBarView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? EnterpriseDetailsViewController {
            vc.enterpriseInfos = self.enterpriseSelected
        }
    }
}

extension ListCompaniesViewController {
    private func hideSearchBarView() {
        UIView.animate(withDuration: 0.5) {
            self.searchBarView.alpha = 0
            self.logoImageNavbar.alpha = 1
            self.searchButton.alpha = 1
        }
    }
    
    private func showSearchBarView() {
        UIView.animate(withDuration: 0.5) {
            self.searchBarView.alpha = 1
            self.logoImageNavbar.alpha = 0
            self.searchButton.alpha = 0
        }
    }
}

extension ListCompaniesViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        DispatchQueue.main.async {
            if textView.text.count <= 0 {
                self.searchSmallLogo.alpha = 0
            } else {
                self.searchSmallLogo.alpha = 1
            }
        }
    }
}

extension ListCompaniesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.enterpriseList.allEnterprise.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "enterpriseCell", for: indexPath) as! CompaniesTableViewCell
        cell.fillCell(withEnterprise: self.enterpriseList.allEnterprise[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.enterpriseSelected = self.enterpriseList.allEnterprise[indexPath.row]
        performSegue(withIdentifier: "showDetailsSegue", sender: self)
    }
    
}
