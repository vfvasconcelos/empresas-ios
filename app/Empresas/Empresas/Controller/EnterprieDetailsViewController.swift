//
//  EnterprieDetailsViewController.swift
//  Empresas
//
//  Created by Victor Vasconcelos on 13/12/19.
//  Copyright © 2019 VictorVasconcelos. All rights reserved.
//

import Foundation
import UIKit

class EnterpriseDetailsViewController: UIViewController {
    @IBOutlet weak var enterpriseImage: UIImageView!
    @IBOutlet weak var enterpriseDescriptionLabel: UILabel!
    @IBOutlet weak var enterpriseName: UINavigationItem!
    public var enterpriseInfos: EnterpriseInfos?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        guard let enterprise = self.enterpriseInfos else { return }
        self.setupContent(enterprise)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    public func setupContent(_ enterprise: EnterpriseInfos) {
        if let photoString = enterprise.photo {
            enterpriseImage.downloaded(from: photoString)
        }
        
        if let description = enterprise.description {
            enterpriseDescriptionLabel.text = description
        } else {
            enterpriseDescriptionLabel.text = "No description"
        }
        
        if let name = enterprise.enterprise_name {
            enterpriseName.title = name
        }
    }
}
