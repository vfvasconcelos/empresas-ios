//
//  UIViewController+Extensions.swift
//  Empresas
//
//  Created by Victor Vasconcelos on 11/12/19.
//  Copyright © 2019 VictorVasconcelos. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tapGesture = UITapGestureRecognizer(target: self,
                         action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
    }

    @objc func hideKeyboard() {
        view.endEditing(true)
    }
}

