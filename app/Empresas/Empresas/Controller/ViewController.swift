//
//  ViewController.swift
//  Empresas
//
//  Created by Victor Vasconcelos on 10/12/19.
//  Copyright © 2019 VictorVasconcelos. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // MARK: Views
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
    }

    // Button Actions
    @IBAction func loginButtonPressed(_ sender: Any) {
        
        guard let email = emailTextField.text,
            let password = passwordTextField.text else { return }
        
        APIManager.shared.loginRequest(
            url: "users/auth/sign_in",
            params: [
                "email" : email,
                "password" : password]) { (result, error) in
                guard error == nil else { return }
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "listCompaniesSegue", sender: self)
                }
                APIManager.shared.getAllEnterpriseRequest() { (result, error) in
                    guard error == nil else { return }
                    
                    guard let data = result else {
                        print("NO data")
                        return
                    }
                    print(data)
                }
        }
        
    }
    
}
