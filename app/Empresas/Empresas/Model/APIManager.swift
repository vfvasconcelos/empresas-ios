//
//  APIManager.swift
//  Empresas
//
//  Created by Victor Vasconcelos on 11/12/19.
//  Copyright © 2019 VictorVasconcelos. All rights reserved.
//

import Foundation

class APIManager {
    public static let shared = APIManager()
    public static let defaultEmail = "testeapple@ioasys.com.br"
    public static let defaultPassword = "12341234"
    
    public let host: String = "https://empresas.ioasys.com.br/api/"
    public let apiVersion: String = "v1"
    private var headers: [String: String] = [:]
    
    func getRequest(url: String,
        completion: @escaping ([String: Any]?, Error?) -> Void){

        guard let URL = URL(string: "\(host)\(apiVersion)/\(url)") else {
            completion(nil, nil)
            return
        }

        let request = NSMutableURLRequest(url: URL)

        request.httpMethod = "GET"
        request.allHTTPHeaderFields = self.headers
      
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            (data, response, error) in
            do {
                
                if let data = data {
                    let response = try JSONSerialization.jsonObject(with: data, options: [])
                    completion(response as? [String : Any], nil)
                }
                else {
                    completion(nil, nil)
                }
            } catch let error as NSError {
                completion(nil, error)
            }
        }
        task.resume()
    }
    
    func getEnterpriseRequest(url: String,
        completion: @escaping (Enterprise?, Error?) -> Void){

        guard let URL = URL(string: "\(host)\(apiVersion)/\(url)") else {
            completion(nil, nil)
            return
        }

        let request = NSMutableURLRequest(url: URL)

        request.httpMethod = "GET"
        request.allHTTPHeaderFields = self.headers
      
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            (data, response, error) in
            do {
                
                if let data = data {
                    let response = try JSONDecoder().decode(Enterprise.self, from: data)
                    completion(response, nil)
                }
                else {
                    completion(nil, nil)
                }
            } catch let error as NSError {
                completion(nil, error)
            }
        }
        task.resume()
    }
    
    func getAllEnterpriseRequest(
            completion: @escaping ([EnterpriseInfos]?, Error?) -> Void){

            guard let URL = URL(string: "\(host)\(apiVersion)/enterprises") else {
                completion(nil, nil)
                return
            }

            let request = NSMutableURLRequest(url: URL)

            request.httpMethod = "GET"
            request.allHTTPHeaderFields = self.headers
          
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                (data, response, error) in
                do {
                    
                    if let data = data {
                        let result = try JSONDecoder().decode([String: [EnterpriseInfos]].self, from: data)
                        completion(result.first?.value, nil)
                    }
                    else {
                        completion(nil, nil)
                    }
                } catch let error as NSError {
                    completion(nil, error)
                }
            }
            task.resume()
        }
    
    func loginRequest(url: String,
                     params: [String: String],
                     completion: @escaping ([String: Any]?, Error?) -> Void){
        
        guard let URL = URL(string: "\(host)\(apiVersion)/\(url)") else {
            completion(nil, nil)
            return
        }

        let request = NSMutableURLRequest(url: URL)

        let postString = params.map { "\($0.0)=\($0.1)" }.joined(separator: "&")

        request.httpMethod = "POST"

        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            (data, response, error) in
            do {
                
                if let urlResponse = response as? HTTPURLResponse {
                    // (access-token, client, uid)
                    if let accessToken = urlResponse.allHeaderFields["access-token"] as? String {
                        self.headers["access-token"] = accessToken
                    }
                    
                    if let client = urlResponse.allHeaderFields["client"] as? String {
                        self.headers["client"] = client
                    }
                    
                    if let uid = urlResponse.allHeaderFields["uid"] as? String {
                        self.headers["uid"] = uid
                    }
                }
                
                if let data = data {
                    let response = try JSONSerialization.jsonObject(with: data, options: [])
                    completion(response as? [String : Any], nil)
                }
                else {
                    completion(nil, nil)
                }
            } catch let error as NSError {
                completion(nil, error)
            }
        }
        task.resume()
    }
    
}
