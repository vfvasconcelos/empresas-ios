//
//  Companie.swift
//  Empresas
//
//  Created by Victor Vasconcelos on 12/12/19.
//  Copyright © 2019 VictorVasconcelos. All rights reserved.
//

import Foundation

class Enterprise: Codable {
    var enterprise: EnterpriseInfos
}

class EnterpriseInfos: Codable {
    var city: String?
    var country: String?
    var description: String?
    var enterprise_name: String?
    var entreprise_type: EnterpriseType?
    var photo: String?
}

struct EnterpriseType: Codable {
    var enterprise_type_name: String?
    var id: Int?
}

class EnterpriseList {
    public static var shared = EnterpriseList()
    
    var allEnterprise: [EnterpriseInfos] = [] {
        didSet {
            NotificationCenter.default.post(name: .newEnterpriseList, object: self)
        }
    }
    
    init() {
        APIManager.shared.getAllEnterpriseRequest { (result, error) in
            guard error == nil else { return }
            guard let data = result else { return }
            EnterpriseList.shared.allEnterprise = data
        }
    }
    
}
