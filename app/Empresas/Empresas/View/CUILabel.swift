//
//  CUILabel.swift
//  Empresas
//
//  Created by Victor Vasconcelos on 10/12/19.
//  Copyright © 2019 VictorVasconcelos. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class CUILabel: UILabel {
    
    @objc enum StyleLabels: Int {
        case textStyle = 0
        case textStyle2
        case textStyle3
        case textStyle4
        case textStyle5
        case typeface
        case typeface2
    }
    
    @IBInspectable var styleAdpeter: Int {
        get {
            return self.style.rawValue
        }
        
        set(value) {
            switch value {
            case 1:
                self.style = .textStyle2
            case 2:
                self.style = .textStyle3
            case 3:
                self.style = .textStyle4
            case 4:
                self.style = .textStyle5
            case 5:
                self.style = .typeface
            case 6:
                self.style = .typeface2
            default:
                self.style = .textStyle
            }
        }
    }
    
    public var style: StyleLabels = .textStyle
    
    override func layoutSubviews() {
        setLabel(basedOnStyle: self.style)
    }
    
    private func setLabel(basedOnStyle style: StyleLabels) {
        switch style {
        case .textStyle:
            self.textColor = .charcoalGrey
            self.font = self.font.withSize(16)
            self.textAlignment = .center
        case .textStyle2:
            self.textColor = .white10
            self.font = self.font.withSize(20)
            self.textAlignment = .center
        case .textStyle3:
            self.textColor = .charcoalGrey
            self.font = self.font.withSize(18)
        case .textStyle4:
            self.textColor = .white10
            self.font = self.font.withSize(15)
            self.textAlignment = .center
        case .textStyle5:
            self.textColor = .charcoalGrey
            self.font = self.font.withSize(15)
            self.textAlignment = .center
        case .typeface:
            self.textColor = .darkIndigo
            self.font = self.font.withSize(18)
        case .typeface2:
            self.textColor = .warmGrey
            self.font = self.font.withSize(15)
        }
    }
}
