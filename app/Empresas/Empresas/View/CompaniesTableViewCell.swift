//
//  CompaniesTableViewCell.swift
//  Empresas
//
//  Created by Victor Vasconcelos on 11/12/19.
//  Copyright © 2019 VictorVasconcelos. All rights reserved.
//

import UIKit

class CompaniesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var companieImage: UIImageView!
    @IBOutlet weak var companieName: CUILabel!
    @IBOutlet weak var companieType: CUILabel!
    @IBOutlet weak var companieLocation: CUILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    public func fillCell(withEnterprise enterprise: EnterpriseInfos) {
        // Fill Enterprise photo
        if let photoString = enterprise.photo {
            companieImage.downloaded(from: photoString)
        }
        
        // Fill Enterprise Name
        companieName.text = enterprise.enterprise_name
        
        // Fill Enterprise Type
        if let enterpriseType = enterprise.entreprise_type {
            companieType.text = enterpriseType.enterprise_type_name
        }
        
        // Fill Enterprise location
        companieLocation.text = enterprise.country
        
    }

}
