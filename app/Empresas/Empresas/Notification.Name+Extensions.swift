//
//  Notification.Name+Extensions.swift
//  Empresas
//
//  Created by Victor Vasconcelos on 12/12/19.
//  Copyright © 2019 VictorVasconcelos. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let newEnterpriseList = Notification.Name("allEnterprisesListUpdated")
}
